# Summary
Ansible role for installing a set of general-purpose command-line tools

# Usage/Included Tools
See which tools are included in the role variables [defaults/main.yml](defaults/main.yml). Control which tools are installed using the per-tool variables (`install_toolname`).

Tools are not installed by default. To install a tool or set of tools, pass `install_toolname: true` when invoking this role from a playbook.

# Supported Platforms
Tested on:
- Mint 21.1
- Kubuntu 22.04

Likely to work with Ubuntu/Debian-based environments that have packages for the tools listed in [defaults/main.yml](defaults/main.yml). Depending on the exact distribution/version/configured package sources, some modifications to [tasks/main.yml](tasks/main.yml) may be needed.