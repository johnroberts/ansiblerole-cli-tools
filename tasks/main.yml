---
- name: Install Zsh
  become: true
  apt:
    state: present
    pkg: zsh
  when: install_zsh

- name: Install Vim
  become: true
  apt:
    state: present
    pkg: vim
  when: install_vim

- name: Install tmux
  become: true
  apt:
    state: present
    pkg: tmux
  when: install_tmux

- name: Install wget
  become: true
  apt:
    state: present
    pkg: wget
  when: install_wget

- name: Install curl
  become: true
  apt:
    state: present
    pkg: curl
  when: install_curl

- name: Install gpg
  become: true
  apt:
    state: present
    pkg: gpg
  when: install_gpg

- name: Install tree
  become: true
  apt:
    state: present
    pkg: tree
  when: install_tree

- name: Install htop
  become: true
  apt:
    state: present
    pkg: htop
  when: install_htop

- name: Install net-tools
  become: true
  apt:
    state: present
    pkg: net-tools
  when: install_net_tools

- name: Install nethogs
  become: true
  apt:
    state: present
    pkg: nethogs
  when: install_nethogs

- name: Install bmon
  become: true
  apt:
    state: present
    pkg: bmon
  when: install_bmon

- name: Install tcpdump
  become: true
  apt:
    state: present
    pkg: tcpdump
  when: install_tcpdump

- name: Install git-secret
  become: true
  apt:
    state: present
    pkg: git-secret
  when: install_git_secret

- name: Add Hashicorp apt signing key
  become: true
  apt_key:
    url: "{{ hashicorp_signing_key_url }}"
    keyring: "{{ hashicorp_keyring_path }}"
    state: present

- name: Add Hashicorp apt repository
  become: true
  apt_repository:
    repo: "{{ hashicorp_repo }}"
    state: present

- name: Install Terraform
  become: true
  apt:
    state: present
    pkg: terraform
  when: install_terraform

- name: Install Packer
  become: true
  apt:
    state: present
    pkg: packer
  when: install_packer

- name: Install Vagrant
  become: true
  apt:
    state: present
    pkg: vagrant
  when: install_vagrant

- name: Install Ansible
  become: true
  apt:
    pkg:
      - ansible
  when: install_ansible

- name: Install ansible-lint
  become: true
  ansible.builtin.package:
    name: ansible-lint
    state: latest
  when: install_ansible_lint

- name: Install ctop (container metrics/monitoring)
  become: true
  apt:
    name: ctop
    state: present
  when: install_ctop

- name: Install zulucrypt CLI
  become: true
  apt:
    name: zulucrypt-cli
    state: present
  when: install_zulucrypt_cli

- name: Install vnStat
  become: true
  apt:
    name: vnstat
    state: present
  when: install_vnstat

- name: Enable and start vnStat service
  become: true
  service:
    name: vnstat
    state: started
    enabled: true
  when: install_vnstat

- name: Install Wireguard
  become: true
  apt:
    pkg:
      - wireguard
      - wireguard-tools
  when: install_wireguard

- name: Install restic
  become: true
  apt:
    pkg: restic
    state: present
  when: install_restic

- name: Perform restic self-update after apt installation
  become: true
  command: restic self-update
  when: install_restic

- name: Add apt-file (for listing apt package files without installing the pkg)
  become: true
  apt:
    name: apt-file
    state: present
  when: install_apt_file

- name: Install tldr (simplified, community manpages from https://tldr.ostera.io/)
  become: true
  apt:
    name: tldr
    state: present
  when: install_tldr

- name: Install rclone
  become: true
  ansible.builtin.package:
    name: rclone
    state: present
  when: install_rclone

- name: Install minikube
  become: true
  ansible.builtin.apt:
    deb: "{{ minikube_deb_url }}"
  when: install_minikube

- name: Add golang PPA (kind prerequisite)
  become: true
  ansible.builtin.apt_repository:
    repo: "{{ kind_golang_ppa }}"
    codename: "{{ kind_golang_ppa_codename }}"
    update_cache: yes
  when: install_kind
  
- name: Install golang (kind prerequisite)
  become: true
  ansible.builtin.apt:
    pkg: golang-go
    state: present
  when: install_kind

- name: Install/upgrade kind (k8s in docker)
  become: true
  ansible.builtin.get_url:
    url: "{{ kind_install_url }}"
    dest: "{{ kind_executable }}"
    mode: '755'
    owner: root
    group: root
    force: true # Download latest kind executable
  when: install_kind

- name: Download and install kubectl binary
  become: true
  ansible.builtin.get_url:
    url: "{{ kubectl_bin_url }}"
    # TODO: look up stable URL, replicating the below in ansible
    #  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    # url: "https://dl.k8s.io/release/{{ lookup('ansible.builtin.url', {{ kubectl_stable_ver_url }}) }}/bin/linux/amd64/kubectl }}"
    dest: /usr/local/bin/kubectl
    mode: '0755'
  when: install_kubectl

# TODO: implement K3d installation
# - name: Download K3d installer script
#   ansible.builtin.get_url:
#     url: "{{ k3d_install_script_url }}"
#     dest: /tmp/k3d_install.sh
#     mode: '0755'
#   when: install_k3d

- name: Install Ansible pip/pipx module prereqs
  become: true
  ansible.builtin.apt:
    pkg:
      - python3-pip
      - python3-setuptools
      - python3-virtualenv
      - pipx
    state: present
  when: install_yt_dlp

- name: Install/update yt-dlp via pipx
  community.general.pipx:
    name: yt-dlp
    state: latest
  when: install_yt_dlp

- name: Install ffmpeg
  become: true
  apt:
    pkg: ffmpeg
    state: present
  when: install_ffmpeg

- name: Install/update semgrep via pipx
  community.general.pipx:
    name: semgrep
    state: present
  when: install_semgrep

- name: Install jq
  become: true
  ansible.builtin.package:
    name: jq
  when: install_jq